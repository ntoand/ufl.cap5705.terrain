#include <stdio.h>
#include "GL/glus.h"
#include "camera.h"
#include "shared.h"

scene_t g_scene;

float g_screenWidth;
float g_screenHeight;

GLfloat g_mMatrix[16];	// Model matrix
GLfloat g_vMatrix[16];	// View matrix
GLfloat g_pMatrix[16];	// Projection matrix
GLfloat g_mvMatrix[16];	// Movel view matrix
GLfloat g_mvpMatrix[16];	// Model-view-projection matrix
GLfloat g_nMatrix[9];      // Normal matrix

float quadData[] = {
	// Vert 1
	-20.0f, 0.0f, -20.0f, 1.0,	// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	0.0f, 2.0f,					// Tex coord (u,v)

	// Vert 2
	20.0f, 0.0f, -20.0f, 1.0,	// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	2.0f, 2.0f,					// Tex coord (u,v)

	// Vert 3
	20.0f, 0.0f, 20.0f, 1.0,	// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	2.0f, 0.0f,					// Tex coord (u,v)

	// Vert 4
	-20.0f, 0.0f, 20.0f, 1.0,	// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	0.0f, 0.0f,					// Tex coord (u,v)
};
GLuint quadIndicies[] = { 0, 3, 2, 0, 2, 1 };
GLuint quadPatchInd[] = { 0, 1, 2, 3 };

GLuint g_vbo_quad; // Vertex buffer object
GLuint g_vao_quad; // Vertex attribute object
GLuint g_ibo_quad; // Index buffer object

#define VMB_PATCH_WIDTH 40.0
#define VMB_TILE_WIDTH 8 // 1 tile = 8 x 8 patches

typedef struct {
	float origin[4];
	float color[3];
} light_t;

light_t light0;


double lastTime;

GLUSboolean init(GLUSvoid)
{
	//
	// LIGHTING
	//
	light0.origin[0] = 100.0f;
	light0.origin[1] = 500.0f;
	light0.origin[2] = 0.0f;
	light0.origin[3] = 1.0f;
	light0.color[0] = 1.0f;
	light0.color[1] = 1.0f;
	light0.color[2] = 1.0f;

	//

	shader_programInit();

	//

	glUseProgram(g_program.program);

	// Setup data
	glGenBuffers(1, &g_vbo_quad);
	glBindBuffer(GL_ARRAY_BUFFER, g_vbo_quad);
	glBufferData(GL_ARRAY_BUFFER, 14 * 4 * sizeof(float), quadData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &g_ibo_quad);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ibo_quad);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(float), quadPatchInd, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &g_vao_quad);
	glBindVertexArray(g_vao_quad);
	glBindBuffer(GL_ARRAY_BUFFER, g_vbo_quad);
	
	glEnableVertexAttribArray(g_program_vertexAttribute);
	glVertexAttribPointer(g_program_vertexAttribute, 4, GL_FLOAT, GL_FALSE, 14 * sizeof(float), 0);
	glEnableVertexAttribArray(g_program_patchTexCoordAttribute);
	glVertexAttribPointer(g_program_patchTexCoordAttribute, 2, GL_FLOAT, GL_FALSE, 14 * sizeof(float), 12 * sizeof(float));
	
	////

//	shader_updateLightBlock(VMB_LIGHTBLOCK_ORIGIN, 4 * sizeof(float), light0.origin);
//	shader_updateLightBlock(VMB_LIGHTBLOCK_COLOR, 3 * sizeof(float), light0.color);

	////

	if (strlen(g_scene.heightMap))
		tex_load(GL_TEXTURE0, 0, g_scene.heightMap, g_program.program, "TexTerrainHeight");
	if (strlen(g_scene.normalMap))
		tex_load(GL_TEXTURE1, 1, g_scene.normalMap, g_scene.normalMap, "TexTerrainNormal");
	if (strlen(g_scene.texBase.colorMap))
		tex_loadMipmap(GL_TEXTURE2, 2, g_scene.texBase.mipLevels, g_scene.texBase.colorMap, g_program.program, "TexBase");
	if (strlen(g_scene.tex0.colorMap))
		tex_loadMipmap(GL_TEXTURE3, 3, g_scene.tex0.mipLevels, g_scene.tex0.colorMap, g_program.program, "Tex0");
	if (strlen(g_scene.tex1.colorMap))
		tex_loadMipmap(GL_TEXTURE4, 4, g_scene.tex1.mipLevels, g_scene.tex1.colorMap, g_program.program, "Tex1");
	if (strlen(g_scene.tex2.colorMap))
		tex_loadMipmap(GL_TEXTURE5, 5, g_scene.tex2.mipLevels, g_scene.tex2.colorMap, g_program.program, "Tex2");
	if (strlen(g_scene.tex3.colorMap))
		tex_loadMipmap(GL_TEXTURE6, 6, g_scene.tex3.mipLevels, g_scene.tex3.colorMap, g_program.program, "Tex3");

	////

	float tileWidth = VMB_TILE_WIDTH * VMB_PATCH_WIDTH;

	GLuint temp = glGetUniformLocation(g_program.program, "TerrainWidth");
	glUniform1f(temp, tileWidth * g_scene.terrainWidth);
	temp = glGetUniformLocation(g_program.program, "TerrainHeight");
	glUniform1f(temp, tileWidth * g_scene.terrainHeight);
	temp = glGetUniformLocation(g_program.program, "TerrainOrigin");
	glUniform2f(temp, 
		-tileWidth * floor(g_scene.terrainWidth / 2) - tileWidth / 2.0,
		-tileWidth * ceil(g_scene.terrainHeight / 2) + tileWidth / 2.0);


	temp = glGetUniformLocation(g_program.program, "ToggleWireframe");
	glUniform1f(temp, 0.0);

	temp = glGetUniformLocation(g_program.program, "SplatHeightSoft");
	glUniform4f(temp, g_scene.tex0.softHeight, g_scene.tex1.softHeight, g_scene.tex2.softHeight, g_scene.tex3.softHeight);
	temp = glGetUniformLocation(g_program.program, "SplatHeightHard");
	glUniform4f(temp, g_scene.tex0.hardHeight, g_scene.tex1.hardHeight, g_scene.tex2.hardHeight, g_scene.tex3.hardHeight);

	temp = glGetUniformLocation(g_program.program, "LightOrigin");
	glUniform4f(temp, light0.origin[0], light0.origin[1], light0.origin[2], light0.origin[3]);
	temp = glGetUniformLocation(g_program.program, "LightColor");
	glUniform3f(temp, light0.color[0], light0.color[1], light0.color[2]);


	///

	//glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	glClearColor(0.3f, 0.3f, 0.5f, 1.0f);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// We work with 4 points per patch.
	glPatchParameteri(GL_PATCH_VERTICES, 4);


	lastTime = glfwGetTime();

	return GLUS_TRUE;
}

GLUSvoid reshape(GLUSint width, GLUSint height)
{
	glViewport(0, 0, width, height);
	g_screenWidth = width;
	g_screenHeight = height;

	GLuint vp = glGetUniformLocation(g_program.program, "Viewport");
	glUniform2f(vp, width, height);


	glusMatrix4x4Perspectivef(g_pMatrix, 40.0f, (GLfloat)width / (GLfloat)height, 1.0f, 10000.0f);

	//glUseProgram(g_programTess.program);
	//shader_updateMatrixBlock(VMB_MATBLOCK_P_OFFSET_INDEX, 16 * sizeof(float), g_pMatrix);

	GLuint temp = glGetUniformLocation(g_program.program, "pMatrix");
	glUniformMatrix4fv(temp, 1, GL_FALSE, g_pMatrix);
}



void drawPatch(float x, float y, float z)
{
	glusMatrix4x4Identityf(g_mvMatrix);
	glusMatrix4x4Identityf(g_mMatrix);
	glusMatrix4x4Translatef(g_mMatrix, x, y, z);
	glusMatrix4x4Multiplyf(g_mvMatrix, g_vMatrix, g_mMatrix);
	//shader_updateMatrixBlock(VMB_MATBLOCK_MV_OFFSET_INDEX, 16 * sizeof(float), g_mvMatrix);
	//shader_updateMatrixBlock(VMB_MATBLOCK_M_OFFSET_INDEX, 16 * sizeof(float), g_mMatrix);
	GLuint temp2 = glGetUniformLocation(g_program.program, "mvMatrix");
	glUniformMatrix4fv(temp2, 1, GL_FALSE, g_mvMatrix);
	temp2 = glGetUniformLocation(g_program.program, "mMatrix");
	glUniformMatrix4fv(temp2, 1, GL_FALSE, g_mMatrix);

	float temp[4] = { x, y, z, 1.0 };
	glusMatrix4x4MultiplyPoint4f(temp, g_mvMatrix, temp);
	glusMatrix4x4MultiplyPoint4f(temp, g_pMatrix, temp);

	//if (temp[0] > -temp[3] ||
	//	temp[0] < temp[3] ||
	//	temp[1] > -temp[3] ||
	//	temp[1] < temp[3] ||
	//	temp[2] > -temp[3] ||
	//	temp[2] < temp[3])
	//{
		// Calc normal matrix
		glusMatrix4x4ExtractMatrix3x3f(g_nMatrix, g_mvMatrix);
		glusMatrix3x3Transposef(g_nMatrix);
		glusMatrix3x3Inversef(g_nMatrix);
		//shader_updateMatrixBlock(VMB_MATBLOCK_N_OFFSET_INDEX, 9 * sizeof(float), g_nMatrix);
		temp2 = glGetUniformLocation(g_program.program, "nMatrix");
		glUniformMatrix3fv(temp2, 1, GL_FALSE, g_nMatrix);

		glDrawElements(GL_PATCHES, 4, GL_UNSIGNED_INT, 0);
	//}

}

// Tile = 8x8 patches
void drawTile(float x, float y, float z)
{
	int row, col;

	for (row = 0; row < VMB_TILE_WIDTH; row++)
	{
		float r = z + (VMB_PATCH_WIDTH * 4.0) - ((float)row * VMB_PATCH_WIDTH) - (VMB_PATCH_WIDTH / 2.0);

		for (col = 0; col < VMB_TILE_WIDTH; col++)
		{
			float c = x - (VMB_PATCH_WIDTH * 4.0) + ((float)col * VMB_PATCH_WIDTH) + (VMB_PATCH_WIDTH / 2.0);
			drawPatch(c, y, r);
		}
	}
}

void drawTerrain(float x, float y, float z, int width, int height)
{
	int row, col;

	for (row = 0; row < height; row++)
	{
		float r = z + (VMB_TILE_WIDTH * VMB_PATCH_WIDTH * floor(height / 2.0)) - ((float)row * VMB_PATCH_WIDTH * VMB_TILE_WIDTH);
	
		for (col = 0; col < width; col++)
		{
			float c = x - (VMB_TILE_WIDTH * VMB_PATCH_WIDTH * floor(width / 2.0)) + ((float)col * VMB_PATCH_WIDTH * VMB_TILE_WIDTH);
			drawTile(c, y, r);
		}
	}
}


	int numFrames = 0;


GLUSboolean update(GLUSfloat time)
{
	numFrames++;
	if (glfwGetTime() - lastTime >= 1.0)
	{
		//printf("%f ms/frame\n", 1000.0 / (double)numFrames);
		printf("%f ms/frame\n", 1000.0 / (double)numFrames);
		numFrames = 0;
		lastTime += 1.0;
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Build view matrix
	cam_buildViewMatrix(g_vMatrix);

	// Draw patch using tessellation program
	glUseProgram(g_program.program);

	glBindVertexArray(g_vao_quad);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ibo_quad);

	//drawTile(0, 0, 0);
	//drawTile(160, 0, 0);
	drawTerrain(0, 0, 0, g_scene.terrainWidth, g_scene.terrainHeight);

	return GLUS_TRUE;
}

GLboolean rightMouseDown = GL_FALSE;
GLboolean leftMouseDown = GL_FALSE;

float cursorX = 0.0f;
float cursorY = 0.0f;

float prevMouseX = 0.0f;
float prevMouseY = 0.0f;
float mouseSen = 0.15; // Mouse Sensitity

GLUSvoid mouse(const GLUSboolean pressed, const GLUSint button, const GLUSint xPos, const GLUSint yPos)
{
	if (!leftMouseDown && !rightMouseDown)
	{
		if (pressed && (button & GLFW_MOUSE_BUTTON_5 || button & GLFW_MOUSE_BUTTON_2))
		{
			// Store previous cursor postion
			cursorX = prevMouseX = xPos;
			cursorY = prevMouseY = yPos;

			glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
	}

	if (pressed && button & GLFW_MOUSE_BUTTON_5)
		rightMouseDown = GL_TRUE;

	if (pressed && button & GLFW_MOUSE_BUTTON_2)
		leftMouseDown = GL_TRUE;


	if (!pressed && button & GLFW_MOUSE_BUTTON_5)
		rightMouseDown = GL_FALSE;

	if (!pressed && button & GLFW_MOUSE_BUTTON_2)
		leftMouseDown = GL_FALSE;


	if (!leftMouseDown && !rightMouseDown)
	{
		glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		glfwSetCursorPos(glfwGetCurrentContext(), cursorX, cursorY);
	}
}

GLUSvoid mouseMove(const GLUSint buttons, const GLUSint xPos, const GLUSint yPos)
{
	// RIGHT + LEFT Mouse buttons
	if (buttons & GLFW_MOUSE_BUTTON_5 && buttons & GLFW_MOUSE_BUTTON_2)
	{
		float vertDelta = (yPos - cursorY) * mouseSen * -0.6;
		float horizDelta = (xPos - cursorX) * mouseSen * 0.6;

		cam_pan(vertDelta, horizDelta);

		prevMouseX = xPos;
		prevMouseY = yPos;
	}

	// RIGHT MB
	else if (buttons & GLFW_MOUSE_BUTTON_5)
	{
		// Calc mouse change
		float rotDeltaY = (xPos - cursorX) * mouseSen * -1;
		float rotDeltaX = (yPos - cursorY) * mouseSen * -0.8;

		cam_rotY(rotDeltaY);
		cam_rotX(rotDeltaX);

		prevMouseX = xPos;
		prevMouseY = yPos;
	}

	// LEFT MB
	else if (buttons & GLFW_MOUSE_BUTTON_2)
	{
		// Calc mouse change
		float rotDeltaY = (xPos - cursorX) * mouseSen * -1;
		float moveDelta = (yPos - cursorY) * mouseSen * -0.8;
		
		cam_rotY(rotDeltaY);
		cam_move(moveDelta);

		prevMouseX = xPos;
		prevMouseY = yPos;
	}

	if (leftMouseDown || rightMouseDown)
		glfwSetCursorPos(glfwGetCurrentContext(), cursorX, cursorY);
}

GLUSvoid terminate(GLUSvoid)
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	if (g_vbo_quad)
	{
		glDeleteBuffers(1, &g_vbo_quad);
		g_vbo_quad = 0;
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	if (g_ibo_quad)
	{
		glDeleteBuffers(1, &g_ibo_quad);
		g_ibo_quad = 0;
	}

	glBindVertexArray(0);
	if (g_vao_quad)
	{
		glDeleteVertexArrays(1, &g_vao_quad);
		g_vao_quad = 0;
	}


	glUseProgram(0);

	glusProgramDestroy(&g_program);
}

float moveSpeed = 20.0;

GLUSvoid keyboard(const GLUSboolean pressed, const GLUSint key)
{
	GLint temp;

	switch (key)
	{
	case 'w': // Cheap movement, but it works
		cam_move(1.0 * moveSpeed);
		break;
	case 's': // Cheap movement, but it works
		cam_move(-1.0 * moveSpeed);
		break;
	case 'a': // Cheap movement, but it works
		cam_pan(0.0, -1.0 * moveSpeed);
		break;
	case 'd': // Cheap movement, but it works
		cam_pan(0.0, 1.0 * moveSpeed);
		break;

	case 'p':

		temp = glGetUniformLocation(g_program.program, "ToggleWireframe");
		glUniform1f(temp, 1.0);
		break;
	case 'o':

		temp = glGetUniformLocation(g_program.program, "ToggleWireframe");
		glUniform1f(temp, 0.0);
		break;
	}

}


int main(int argc, char* argv[])
{
	if (argc > 1)
		file_loadScene(argv[1], &g_scene);
	else
		file_loadScene("../../data/u_canyon.txt", &g_scene);

	EGLint eglConfigAttributes[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_DEPTH_SIZE, 24,
		EGL_STENCIL_SIZE, 0,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
		EGL_NONE
	};

	EGLint eglContextAttributes[] = {
		EGL_CONTEXT_MAJOR_VERSION, 4,
		EGL_CONTEXT_MINOR_VERSION, 2,
		EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE, EGL_TRUE,
		EGL_CONTEXT_OPENGL_PROFILE_MASK, EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
		EGL_NONE
	};

	glusWindowSetInitFunc(init);
	glusWindowSetReshapeFunc(reshape);
	glusWindowSetUpdateFunc(update);
	glusWindowSetTerminateFunc(terminate);
	glusWindowSetMouseFunc(mouse);
	glusWindowSetMouseMoveFunc(mouseMove);
	glusWindowSetKeyFunc(keyboard);

	if (!glusWindowCreate("VMB Terrain (Uniform Patch)", 1024, 768, GLUS_FALSE, GLUS_FALSE, eglConfigAttributes, eglContextAttributes))
	{
		printf("Could not create window!\n");
		return -1;
	}

	glusWindowRun();

	return 0;
}