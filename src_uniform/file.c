#include <stdio.h>
#include <stdlib.h>
#include "GL/glus.h"
#include "shared.h"

void file_parseKey(char *key, char *val, scene_t *scene)
{
	if (!strcmp(key, "TerrainWidth"))
		scene->terrainWidth = atof(val);
	else if (!strcmp(key, "TerrainHeight"))
		scene->terrainHeight = atof(val);

	else if (!strcmp(key, "HeightMap"))
		strcpy_s(scene->heightMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "NormalMap"))
		strcpy_s(scene->normalMap, 50 * sizeof(char), val);

	else if (!strcmp(key, "TexBase"))
		strcpy_s(scene->texBase.colorMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "TexBaseNormal"))
		strcpy_s(scene->texBase.normalMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "TexBaseMipLevels"))
		scene->texBase.mipLevels = atoi(val);
	else if (!strcmp(key, "TexBaseSoftHeight"))
		scene->texBase.softHeight = atof(val);
	else if (!strcmp(key, "TexBaseHardHeight"))
		scene->texBase.hardHeight = atof(val);

	else if (!strcmp(key, "Tex0"))
		strcpy_s(scene->tex0.colorMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex0Normal"))
		strcpy_s(scene->tex0.normalMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex0MipLevels"))
		scene->tex0.mipLevels = atoi(val);
	else if (!strcmp(key, "Tex0SoftHeight"))
		scene->tex0.softHeight = atof(val);
	else if (!strcmp(key, "Tex0HardHeight"))
		scene->tex0.hardHeight = atof(val);

	else if (!strcmp(key, "Tex1"))
		strcpy_s(scene->tex1.colorMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex1Normal"))
		strcpy_s(scene->tex1.normalMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex1MipLevels"))
		scene->tex1.mipLevels = atoi(val);
	else if (!strcmp(key, "Tex1SoftHeight"))
		scene->tex1.softHeight = atof(val);
	else if (!strcmp(key, "Tex1HardHeight"))
		scene->tex1.hardHeight = atof(val);

	else if (!strcmp(key, "Tex2"))
		strcpy_s(scene->tex2.colorMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex2Normal"))
		strcpy_s(scene->tex2.normalMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex2MipLevels"))
		scene->tex2.mipLevels = atoi(val);
	else if (!strcmp(key, "Tex2SoftHeight"))
		scene->tex2.softHeight = atof(val);
	else if (!strcmp(key, "Tex2HardHeight"))
		scene->tex2.hardHeight = atof(val);

	else if (!strcmp(key, "Tex3"))
		strcpy_s(scene->tex3.colorMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex3Normal"))
		strcpy_s(scene->tex3.normalMap, 50 * sizeof(char), val);
	else if (!strcmp(key, "Tex3MipLevels"))
		scene->tex3.mipLevels = atoi(val);
	else if (!strcmp(key, "Tex3SoftHeight"))
		scene->tex3.softHeight = atof(val);
	else if (!strcmp(key, "Tex3HardHeight"))
		scene->tex3.hardHeight = atof(val);

	else
		printf("%s: %s\n", "Error loading scene file. Invalid key", key);
}

char *file_readLine(char *f, char* buffer)
{
	int i = 0;

	while (*f && *f != '\n' && *f != '\r' && i < 49)
	{
		buffer[i] = *f;
		i++;
		f++;
	}
	buffer[i] = '\0';

	if (*f == '\r')
		f++;

	if (*f == '\n')
		f++;

	return f;
}

float file_initScene(scene_t *scene)
{
	scene->terrainHeight = 1.0;
	scene->terrainWidth = 1.0;

	scene->heightMap[0] = '\0';
	scene->normalMap[0] = '\0';

	scene->texBase.colorMap[0] = '\0';
	scene->texBase.normalMap[0] = '\0';
	scene->texBase.mipLevels = 3;
	scene->texBase.softHeight = 0.0;
	scene->texBase.hardHeight = 0.0;

	scene->tex0.colorMap[0] = '\0';
	scene->tex0.normalMap[0] = '\0';
	scene->tex0.mipLevels = 3;
	scene->tex0.softHeight = 0.0;
	scene->tex0.hardHeight = 0.0;

	scene->tex1.colorMap[0] = '\0';
	scene->tex1.normalMap[0] = '\0';
	scene->tex1.mipLevels = 3;
	scene->tex1.softHeight = 0.0;
	scene->tex1.hardHeight = 0.0;

	scene->tex2.colorMap[0] = '\0';
	scene->tex2.normalMap[0] = '\0';
	scene->tex2.mipLevels = 3;
	scene->tex2.softHeight = 0.0;
	scene->tex2.hardHeight = 0.0;

	scene->tex3.colorMap[0] = '\0';
	scene->tex3.normalMap[0] = '\0';
	scene->tex3.mipLevels = 3;
	scene->tex3.softHeight = 0.0;
	scene->tex3.hardHeight = 0.0;
}

int file_loadScene(char* sceneFile, scene_t *scene)
{
	file_initScene(scene);

	GLUStextfile file;
	glusFileLoadText(sceneFile, &file);

	char *f = file.text;
	char key[50];
	char val[50];
	int i = 0;

	while (*f)
	{
		f = file_readLine(f, key);
		f = file_readLine(f, val);

		file_parseKey(key, val, scene);
	}

	glusFileDestroyText(&file);

	// Validation
	if (scene->terrainHeight == 0.0)
		scene->terrainHeight = 1.0;
	if (scene->terrainWidth == 0.0)
		scene->terrainWidth = 1.0;

	return 1;
}