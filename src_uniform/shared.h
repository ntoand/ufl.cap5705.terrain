//
// Main
//
extern GLfloat g_mMatrix[16];	// Model matrix
extern GLfloat g_vMatrix[16];	// View matrix
extern GLfloat g_pMatrix[16];	// Projection matrix
extern GLfloat g_mvMatrix[16];	// Movel view matrix
extern GLfloat g_mvpMatrix[16];	// Model-view-projection matrix

//
// Shader.c
//
extern GLUSprogram g_program;
extern GLuint g_program_vertexAttribute;
extern GLuint g_program_patchTexCoordAttribute;

void shader_programInit();
void shader_updateMatrixBlock(int offsetIndex, GLsizeiptr size, const void *data);
void shader_updateLightBlock(int offsetIndex, GLsizeiptr size, const void *data);

// Matrix uniform block indices
#define VMB_MATBLOCK_M_OFFSET_INDEX 0    // Normal matrix
#define VMB_MATBLOCK_P_OFFSET_INDEX 1    // Projection matrix
#define VMB_MATBLOCK_MV_OFFSET_INDEX 2   // Model-view matrix
#define VMB_MATBLOCK_MVP_OFFSET_INDEX 3  // Model-view-projection matrix
#define VMB_MATBLOCK_N_OFFSET_INDEX 4    // Normal matrix

// Light uniform block indices
#define VMB_LIGHTBLOCK_ORIGIN 0  // Light origin
#define VMB_LIGHTBLOCK_COLOR 1   // Light color


//
// texture.c
//
void tex_load(GLenum texture, int texNum, char *file, GLuint program, char* uniform);
void tex_loadMipmap(GLenum texture, int texNum, int mipLevels, char *file, GLuint program, char* uniform);


typedef struct {
	char colorMap[50];
	char normalMap[50];
	int mipLevels;
	float softHeight;
	float hardHeight;

} tex_terrain_t;

//
//
//
typedef struct {
	char heightMap[50];
	char normalMap[50];

	tex_terrain_t texBase; // Base texture.
	tex_terrain_t tex0;
	tex_terrain_t tex1;
	tex_terrain_t tex2;
	tex_terrain_t tex3;

	int terrainWidth;  // Number of tiles
	int terrainHeight; // Number of tiles
} scene_t;

//
// file.c
//
int file_loadScene(char* sceneFile, scene_t *scene);