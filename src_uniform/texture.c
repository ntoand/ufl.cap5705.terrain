#include <stdio.h>
#include "GL/glus.h"
#include "shared.h"

/**
* Loads texture without generating mipmaps.
* GLenum texture - OpenGL texture to use, i.e. GL_TEXTURE0.
* int texNum - Number corresponding to the texture parameter.
* char *file - path to the texture file.
* GLuint program - the shader program to use the texture in.
* char *uniform - name of the sampler2D uniform in the shader.
*/
void tex_load(GLenum texture, int texNum, char *file, GLuint program, char* uniform)
{
	GLUStgaimage image;
	if (glusImageLoadTga(file, &image))
	{
		printf("Texture loaded: %s\n", file);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glActiveTexture(texture);

		GLuint tid;
		glGenTextures(1, &tid);
		glBindTexture(GL_TEXTURE_2D, tid);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, image.width, image.height);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.width, image.height, image.format, GL_UNSIGNED_BYTE, image.data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		int loc = glGetUniformLocation(program, uniform);
		if (loc >= texNum)
			glUniform1i(loc, texNum);
	}
	else
	{
		printf("Error loading texture: %s\n", file);
	}

	glusImageDestroyTga(&image);
}

/**
* Loads texture and generates mipmaps.
* GLenum texture - OpenGL texture to use, i.e. GL_TEXTURE0.
* int texNum - Number corresponding to the texture parameter.
* int mipLevels - Number of mipmaps to generate.
* char *file - path to the texture file.
* GLuint program - the shader program to use the texture in.
* char *uniform - name of the sampler2D uniform in the shader.
*/
void tex_loadMipmap(GLenum texture, int texNum, int mipLevels, char *file, GLuint program, char* uniform)
{
	GLUStgaimage image;
	if (glusImageLoadTga(file, &image))
	{
		printf("Texture loaded: %s\n", file);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glActiveTexture(texture);

		GLuint tid;
		glGenTextures(1, &tid);
		glBindTexture(GL_TEXTURE_2D, tid);
		glTexStorage2D(GL_TEXTURE_2D, mipLevels, GL_RGB8, image.width, image.height);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.width, image.height, image.format, GL_UNSIGNED_BYTE, image.data);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		int loc = glGetUniformLocation(program, uniform);
		if (loc >= texNum)
			glUniform1i(loc, texNum);
	}
	else
	{
		printf("Error loading texture: %s\n", file);
	}

	glusImageDestroyTga(&image);
}