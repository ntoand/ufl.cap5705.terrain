#include <stdio.h>
#include "GL/glus.h"

float camPos[4] = { 0, 1, 5, 1 };

float camRight[4] = { 1, 0, 0, 0 };
float camUp[4] = { 0, 1, 0, 0 };
float camDir[4] = { 0, 0, -1, 0 };

float camRotX = 0.0;
float camRotY = 180.0;

void cam_pan(float vertDelta, float horizDelta)
{
	float newPos[3] = { 0, 0, 0 };
	float newPos2[3] = { 0, 0, 0 };

	glusVector3MultiplyScalarf(newPos, camRight, horizDelta);
	glusVector3MultiplyScalarf(newPos2, camUp, vertDelta);

	glusPoint4AddVector3f(camPos, camPos, newPos);
	glusPoint4AddVector3f(camPos, camPos, newPos2);
}

void cam_move(float moveDelta)
{
	float newPos[4] = { 0, 0, 0, 1 };
	float temp[4] = { camDir[0], 0, camDir[2] }; // Movement only occurs on the X / Z axes. Kill movement on Y axis.
	glusVector3MultiplyScalarf(newPos, temp, moveDelta);
	glusPoint4AddVector3f(camPos, camPos, newPos);
}

void cam_rotY(float rotDeltaY)
{
	camRotY += rotDeltaY;

	// Update camera
	if (camRotY > 360)
		camRotY = 0;// +(camRotY - 360);
	if (camRotY < 0)
		camRotY = 360;// +camRotY;

	//printf("Rot Y: %f\n", camRotY * 3.14159 / 180);

	// Calc new direction (note conversion from degrees to radians)
	camDir[0] = cos(camRotX * 3.14159 / 180) * sin(camRotY * 3.14159 / 180);
	camDir[1] = sin(camRotX * 3.14159 / 180);
	camDir[2] = cos(camRotX * 3.14159 / 180) * cos(camRotY * 3.14159 / 180);

	// Update cam right vector
	glusVector3Crossf(camRight, camDir, camUp);
}

void cam_rotX(float rotDeltaX)
{
	camRotX += rotDeltaX;

	// Update camera
	if (camRotX > 90)
		camRotX = 90;// +(camRotY - 360);
	if (camRotX < -90)
		camRotX = -90;// +camRotY;

	//printf("Rot X: %f degrees, %f\n", camRotX, camRotX * 3.14159 / 180);

	// Calc new direction (note conversion from degrees to radians)
	camDir[0] = cos(camRotX * 3.14159 / 180) * sin(camRotY * 3.14159 / 180);
	camDir[1] = sin(camRotX * 3.14159 / 180);
	camDir[2] = cos(camRotX * 3.14159 / 180) * cos(camRotY * 3.14159 / 180);

	// Update cam right vector
	glusVector3Crossf(camRight, camDir, camUp);
}

void cam_buildViewMatrix(float viewMatrix[16])
{
	float lookat[4];

	// Calculate point the camera is looking at
	glusVector3AddVector3f(lookat, camPos, camDir);

	// Construct the view matrix
	glusMatrix4x4LookAtf(viewMatrix, 
		camPos[0], camPos[1], camPos[2], 
		lookat[0], lookat[1], lookat[2], 
		camUp[0], camUp[1], camUp[2]);
}