#include <stdio.h>
#include "GL/glus.h"
#include "camera.h"
#include "shared.h"
#include "terrain.h"
#include "skybox.h"

scene_t g_scene;

float g_screenWidth;
float g_screenHeight;

GLfloat g_mMatrix[16];	    // Model matrix
GLfloat g_vMatrix[16];	    // View matrix
GLfloat g_pMatrix[16];	    // Projection matrix
GLfloat g_mvMatrix[16];	    // Movel view matrix
GLfloat g_mvpMatrix[16];	// Model-view-projection matrix
GLfloat g_nMatrix[9];       // Normal matrix

float quadData[] = {
	// Vert 1
	-1.0f, 0.0f, -1.0f, 1.0,	// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	0.0f, 2.0f,					// Tex coord (u,v)

	// Vert 2
	1.0f, 0.0f, -1.0f, 1.0,		// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	2.0f, 2.0f,					// Tex coord (u,v)

	// Vert 3
	1.0f, 0.0f, 1.0f, 1.0,		// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	2.0f, 0.0f,					// Tex coord (u,v)

	// Vert 4
	-1.0f, 0.0f, 1.0f, 1.0,		// Position
	0.18f, 0.91f, 0.46f, 1.0,	// Color
	0.0f, 1.0f, 0.0f, 0.0f,		// Normal
	0.0f, 0.0f,					// Tex coord (u,v)
};
GLuint quadIndicies[] = { 0, 3, 2, 0, 2, 1 };
GLuint quadPatchInd[] = { 0, 1, 2, 3 };

GLuint g_vbo_quad; // Vertex buffer object
GLuint g_vao_quad; // Vertex attribute object
GLuint g_ibo_quad; // Index buffer object

// Framerate junk
int numFrames = 0;
double lastTime;

int showSkybox = 1;


GLUSboolean init(GLUSvoid)
{
	terrain_init();

	//

	shader_programInit();
	shader_skyboxProgramInit();

	//

	glUseProgram(g_program.program);

	// Setup data
	glGenBuffers(1, &g_vbo_quad);
	glBindBuffer(GL_ARRAY_BUFFER, g_vbo_quad);
	glBufferData(GL_ARRAY_BUFFER, 14 * 4 * sizeof(float), quadData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &g_ibo_quad);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ibo_quad);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(float), quadPatchInd, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &g_vao_quad);
	glBindVertexArray(g_vao_quad);
	glBindBuffer(GL_ARRAY_BUFFER, g_vbo_quad);
	
	glEnableVertexAttribArray(g_program_vertexAttribute);
	glVertexAttribPointer(g_program_vertexAttribute, 4, GL_FLOAT, GL_FALSE, 14 * sizeof(float), 0);

	////

	if (strlen(g_scene.heightMap))
		tex_loadMipmap(GL_TEXTURE0, 0, 5, g_scene.heightMap, g_program.program, "TexTerrainHeight");
	tex_loadMipmap(GL_TEXTURE1, 1, 5, "../../dev/terrain/ASTGDEMV2_0N38W107.tga", g_program.program, "TexTerrainHeight2");

	///

	GLuint temp = glGetUniformLocation(g_program.program, "TerrainLength");
	glUniform1f(temp, g_scene.terrainLength);
	temp = glGetUniformLocation(g_program.program, "TerrainWidth");
	glUniform1f(temp, g_scene.terrainWidth);
	temp = glGetUniformLocation(g_program.program, "TerrainOrigin");
	glUniform3f(temp, -g_scene.terrainWidth / 2.0, 0.0, -g_scene.terrainLength / 2.0);
	temp = glGetUniformLocation(g_program.program, "TerrainHeightOffset");
	glUniform1f(temp, g_scene.terrainHeight);

	////

	skybox_init();

	/////

	glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
//	glClearColor(0.3f, 0.3f, 0.5f, 1.0f);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// We work with 4 points per patch.
	glUseProgram(g_program.program);
	glPatchParameteri(GL_PATCH_VERTICES, 4);

	lastTime = glfwGetTime();

	return GLUS_TRUE;
}

/**
* Handles when the window is resized.
*/
GLUSvoid reshape(GLUSint width, GLUSint height)
{
	glViewport(0, 0, width, height);
	g_screenWidth = width;
	g_screenHeight = height;

	// Send the viewport information to the tessellation program
	glUseProgram(g_program.program);
	GLuint vp = glGetUniformLocation(g_program.program, "Viewport");
	glUniform2f(vp, width, height);

	// Update the projection matrix
	glusMatrix4x4Perspectivef(g_pMatrix, 40.0f, (GLfloat)width / (GLfloat)height, 1.0f, 200000.0);

	// Send projection matrix to the tessellation program
	GLuint temp = glGetUniformLocation(g_program.program, "pMatrix");
	glUniformMatrix4fv(temp, 1, GL_FALSE, g_pMatrix);

	// Send the projection matrix to the skybox program
	glUseProgram(g_skyboxProgram.program);
	temp = glGetUniformLocation(g_skyboxProgram.program, "pMatrix");
	glUniformMatrix4fv(temp, 1, GL_FALSE, g_pMatrix);
}


GLUSboolean update(GLUSfloat time)
{
	numFrames++;
	if (glfwGetTime() - lastTime >= 1.0)
	{
		printf("%f ms/frame\n", 1000.0 / (double)numFrames);
		numFrames = 0;
		lastTime += 1.0;
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Build view matrix
	cam_buildViewMatrix(g_vMatrix);

	// Draw skybox
	if (showSkybox)
		skybox_render();

	// Draw patch using tessellation program
	glUseProgram(g_program.program);

	glBindVertexArray(g_vao_quad);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ibo_quad);

//	GLint queryResult;
//	GLuint query;
//	glGenQueries(1, &query);
//	glBeginQuery(GL_PRIMITIVES_GENERATED, query);
	
	// Calculate and render the terrain
	terrain_createTree(0, 0, 0, g_scene.terrainWidth, g_scene.terrainLength);
	terrain_render();

//	glEndQuery(GL_PRIMITIVES_GENERATED);
//	glGetQueryObjectiv(query, GL_QUERY_RESULT, &queryResult);
//	printf("Primitives: %d\n", queryResult);

	return GLUS_TRUE;
}

GLboolean rightMouseDown = GL_FALSE;
GLboolean leftMouseDown = GL_FALSE;

float cursorX = 0.0f;
float cursorY = 0.0f;

float prevMouseX = 0.0f;
float prevMouseY = 0.0f;
float mouseSen = 0.15; // Mouse Sensitity

GLUSvoid mouse(const GLUSboolean pressed, const GLUSint button, const GLUSint xPos, const GLUSint yPos)
{
	if (!leftMouseDown && !rightMouseDown)
	{
		if (pressed && (button & GLFW_MOUSE_BUTTON_5 || button & GLFW_MOUSE_BUTTON_2))
		{
			// Store previous cursor postion
			cursorX = prevMouseX = xPos;
			cursorY = prevMouseY = yPos;

			glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
	}

	if (pressed && button & GLFW_MOUSE_BUTTON_5)
		rightMouseDown = GL_TRUE;

	if (pressed && button & GLFW_MOUSE_BUTTON_2)
		leftMouseDown = GL_TRUE;


	if (!pressed && button & GLFW_MOUSE_BUTTON_5)
		rightMouseDown = GL_FALSE;

	if (!pressed && button & GLFW_MOUSE_BUTTON_2)
		leftMouseDown = GL_FALSE;


	if (!leftMouseDown && !rightMouseDown)
	{
		glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		glfwSetCursorPos(glfwGetCurrentContext(), cursorX, cursorY);
	}
}

GLUSvoid mouseMove(const GLUSint buttons, const GLUSint xPos, const GLUSint yPos)
{
	// RIGHT + LEFT Mouse buttons
	if (buttons & GLFW_MOUSE_BUTTON_5 && buttons & GLFW_MOUSE_BUTTON_2)
	{
		float vertDelta = (yPos - cursorY) * mouseSen * -0.6;
		float horizDelta = (xPos - cursorX) * mouseSen * 0.6;

		cam_pan(vertDelta, horizDelta);

		prevMouseX = xPos;
		prevMouseY = yPos;
	}

	// RIGHT MB
	else if (buttons & GLFW_MOUSE_BUTTON_5)
	{
		// Calc mouse change
		float rotDeltaY = (xPos - cursorX) * mouseSen * -1;
		float rotDeltaX = (yPos - cursorY) * mouseSen * -0.8;

		cam_rotY(rotDeltaY);
		cam_rotX(rotDeltaX);

		prevMouseX = xPos;
		prevMouseY = yPos;
	}

	// LEFT MB
	else if (buttons & GLFW_MOUSE_BUTTON_2)
	{
		// Calc mouse change
		float rotDeltaY = (xPos - cursorX) * mouseSen * -1;
		float moveDelta = (yPos - cursorY) * mouseSen * -0.8;
		
		cam_rotY(rotDeltaY);
		cam_move(moveDelta);

		prevMouseX = xPos;
		prevMouseY = yPos;
	}

	if (leftMouseDown || rightMouseDown)
		glfwSetCursorPos(glfwGetCurrentContext(), cursorX, cursorY);
}

GLUSvoid terminate(GLUSvoid)
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	if (g_vbo_quad)
	{
		glDeleteBuffers(1, &g_vbo_quad);
		g_vbo_quad = 0;
	}
	if (g_vbo_skybox)
	{
		glDeleteBuffers(1, &g_vbo_skybox);
		g_vbo_skybox = 0;
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	if (g_ibo_quad)
	{
		glDeleteBuffers(1, &g_ibo_quad);
		g_ibo_quad = 0;
	}
	if (g_ibo_skybox)
	{
		glDeleteBuffers(1, &g_ibo_skybox);
		g_ibo_skybox = 0;
	}

	glBindVertexArray(0);
	if (g_vao_quad)
	{
		glDeleteVertexArrays(1, &g_vao_quad);
		g_vao_quad = 0;
	}
	if (g_vao_skybox)
	{
		glDeleteVertexArrays(1, &g_vao_skybox);
		g_vao_skybox = 0;
	}

	glUseProgram(0);

	glusProgramDestroy(&g_program);
	glusProgramDestroy(&g_skyboxProgram);

	terrain_shutdown();
}

float moveSpeed = 100.0;
extern int maxRenderDepth;

GLUSvoid keyboard(const GLUSboolean pressed, const GLUSint key)
{
	GLint temp;

	if (!pressed)
		return;

	switch (key)
	{
	case 'w': // Cheap movement, but it works
		cam_move(1.0 * moveSpeed);
		break;
	case 's': // Cheap movement, but it works
		cam_move(-1.0 * moveSpeed);
		break;
	case 'a': // Cheap movement, but it works
		cam_pan(0.0, -1.0 * moveSpeed);
		break;
	case 'd': // Cheap movement, but it works
		cam_pan(0.0, 1.0 * moveSpeed);
		break;
	case 'e':
		cam_pan(1.0 * moveSpeed, 0.0);
		break;
	case 'q':
		cam_pan(-1.0 * moveSpeed, 0.0);
		break;
	case 'm':
		maxRenderDepth++;
		printf("Render depth: %d\n", maxRenderDepth);
		break;
	case 'n':
		maxRenderDepth--;
		printf("Render depth: %d\n", maxRenderDepth);
		break;

	case 'p':
		temp = glGetUniformLocation(g_program.program, "ToggleWireframe");
		glUniform1f(temp, 1.0);
		break;
	case 'o':
		temp = glGetUniformLocation(g_program.program, "ToggleWireframe");
		glUniform1f(temp, 0.0);
		break;

	case 'u':
		showSkybox = showSkybox ? 0 : 1;
		break;
	}

}

int main(int argc, char* argv[])
{
	if (argc > 1)
		file_loadScene(argv[1], &g_scene);
	else
		file_loadScene("../../data/nu_colorado.txt", &g_scene);

	EGLint eglConfigAttributes[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_DEPTH_SIZE, 24,
		EGL_STENCIL_SIZE, 0,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
		EGL_NONE
	};

	EGLint eglContextAttributes[] = {
		EGL_CONTEXT_MAJOR_VERSION, 4,
		EGL_CONTEXT_MINOR_VERSION, 2,
		EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE, EGL_TRUE,
		EGL_CONTEXT_OPENGL_PROFILE_MASK, EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
		EGL_NONE
	};

	glusWindowSetInitFunc(init);
	glusWindowSetReshapeFunc(reshape);
	glusWindowSetUpdateFunc(update);
	glusWindowSetTerminateFunc(terminate);
	glusWindowSetMouseFunc(mouse);
	glusWindowSetMouseMoveFunc(mouseMove);
	glusWindowSetKeyFunc(keyboard);

	if (!glusWindowCreate("VMB Terrain (Non-Uniform Patch)", 1024, 768, GLUS_FALSE, GLUS_FALSE, eglConfigAttributes, eglContextAttributes))
	{
		printf("Could not create window!\n");
		return -1;
	}

	glusWindowRun();

	return 0;
}