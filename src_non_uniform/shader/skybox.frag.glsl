#version 420

//
// Uniforms
//
uniform samplerCube TexSkybox;

//
// Inputs
//
in vec3 vs_texCoord;

//
// Ouputs
//
layout(location = 0, index = 0) out vec4 fragColor;

void main(void)
{
	fragColor = texture(TexSkybox, vs_texCoord);
	//fragColor = vec4(0.0, 0.0, 1.0, 1.0);
}