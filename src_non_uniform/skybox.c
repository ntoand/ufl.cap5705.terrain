#include <stdio.h>
#include "GL/glus.h"
#include "skybox.h"
#include "shared.h"

float skyboxData[] = {

	-10.0f, -10.0f, -10.0f, 1.0,
	10.0f, -10.0f, -10.0f, 1.0,
	10.0f, 10.0f, -10.0f, 1.0,	
	-10.0f, 10.0f, -10.0f, 1.0,

	-10.0f, -10.0f, 10.0f, 1.0,
	10.0f, -10.0f, 10.0f, 1.0,
	10.0f, 10.0f, 10.0f, 1.0,
	-10.0f, 10.0f, 10.0f, 1.0,
};

GLuint skyboxIndices[] = { 
	// Neg Z face
	0, 1, 2,
	0, 2, 3,

	// Neg X face
	4, 0, 3,
	4, 3, 7,

	// Pos Z face
	4, 7, 5,
	5, 7, 6,

	// Pos X face
	5, 6, 2,
	1, 5, 2,

	// Pos y face
	3, 6, 7,
	3, 2, 6,

	// Neg y face
	0, 4, 5,
	0, 5, 1
};

GLuint g_vbo_skybox; // Vertex buffer object
GLuint g_vao_skybox; // Vertex attribute object
GLuint g_ibo_skybox; // Index buffer object

void skybox_init()
{
	glUseProgram(g_skyboxProgram.program);

	// Setup data
	glGenBuffers(1, &g_vbo_skybox);
	glBindBuffer(GL_ARRAY_BUFFER, g_vbo_skybox);
	glBufferData(GL_ARRAY_BUFFER, 32 * sizeof(float), skyboxData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &g_ibo_skybox);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ibo_skybox);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(GLuint), skyboxIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &g_vao_skybox);
	glBindVertexArray(g_vao_skybox);
	glBindBuffer(GL_ARRAY_BUFFER, g_vbo_skybox);

	glEnableVertexAttribArray(g_skyboxProgram_vertexAttribute);
	glVertexAttribPointer(g_skyboxProgram_vertexAttribute, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);

	tex_loadSkybox(GL_TEXTURE2, 2,
		"../../data/skyboxPosX.tga", "../../data/skyboxNegX.tga",
		"../../data/skyboxPosY.tga", "../../data/skyboxNegY.tga",
		"../../data/skyboxPosZ.tga", "../../data/skyboxNegZ.tga",
		g_skyboxProgram.program, "TexSkybox");
}

extern float camPos[4];

void skybox_render()
{
	glDisable(GL_DEPTH_TEST);

	glUseProgram(g_skyboxProgram.program);
	glBindVertexArray(g_vao_skybox);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ibo_skybox);

	glusMatrix4x4Identityf(g_mvMatrix);
	glusMatrix4x4Identityf(g_mMatrix);
	glusMatrix4x4Translatef(g_mMatrix, camPos[0], camPos[1] + 1.5, camPos[2]);
	glusMatrix4x4Multiplyf(g_mvMatrix, g_vMatrix, g_mMatrix);

	GLuint matrix = glGetUniformLocation(g_skyboxProgram.program, "mvMatrix");
	glUniformMatrix4fv(matrix, 1, GL_FALSE, g_mvMatrix);

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

	glEnable(GL_DEPTH_TEST);
}