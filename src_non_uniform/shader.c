#include <stdio.h>
#include "GL/glus.h"
#include "shared.h"

GLuint g_ubo_matrixBlock;
GLint g_ubo_matrixBlock_offsets[4];

GLuint g_ubo_lightBlock;
GLint g_ubo_lightBlock_offsets[2];



//
// Tessellation program
//
GLUSprogram g_program;
GLuint g_program_vertexAttribute;
GLuint g_program_patchTexCoordAttribute;

void shader_programInit()
{
	GLUStextfile vert, tcs, tes, gs, frag;

	if (!glusFileLoadText("../shader/tess.vert.glsl", &vert)) printf("Error loading shader source: %s\n", "../shader/tess.vert.glsl");
	if (!glusFileLoadText("../shader/tess.tcs.glsl", &tcs)) printf("Error loading shader source: %s\n", "../shader/tess.tcs.glsl");
	if (!glusFileLoadText("../shader/tess.tes.glsl", &tes)) printf("Error loading shader source: %s\n", "../shader/tess.tes.glsl");
	if (!glusFileLoadText("../shader/tess.gs.glsl", &gs)) printf("Error loading shader source: %s\n", "../shader/tess.gs.glsl");
	if (!glusFileLoadText("../shader/tess.frag.glsl", &frag)) printf("Error loading shader source: %s\n", "../shader/tess.frag.glsl");

	if (!glusProgramBuildFromSource(&g_program, 
		(const GLUSchar**)&vert.text, 
		(const GLUSchar**)&tcs.text,
		(const GLUSchar**)&tes.text,
		(const GLUSchar**)&gs.text,
		(const GLUSchar**)&frag.text))
		printf("Error compiling shader program.\n");

	glusFileDestroyText(&vert);
	glusFileDestroyText(&tcs);
	glusFileDestroyText(&tes);
	glusFileDestroyText(&gs);
	glusFileDestroyText(&frag);

	glUseProgram(g_program.program);
	
	//
	// Setup attributes
	//
	g_program_vertexAttribute = glGetAttribLocation(g_program.program, "a_vertex");
}

//
// Skybox program
//
GLUSprogram g_skyboxProgram;
GLuint g_skyboxProgram_vertexAttribute;

void shader_skyboxProgramInit()
{
	GLUStextfile vert, frag;

	if (!glusFileLoadText("../shader/skybox.vert.glsl", &vert)) printf("Error loading shader source: %s\n", "../shader/skybox.vert.glsl");
	if (!glusFileLoadText("../shader/skybox.frag.glsl", &frag)) printf("Error loading shader source: %s\n", "../shader/skybox.frag.glsl");

	if (!glusProgramBuildFromSource(&g_skyboxProgram,
		(const GLUSchar**)&vert.text,
		NULL, NULL, NULL,
		(const GLUSchar**)&frag.text))
		printf("Error compiling shader program.\n");

	glusFileDestroyText(&vert);
	glusFileDestroyText(&frag);

	glUseProgram(g_skyboxProgram.program);

	//
	// Setup attributes
	//
	g_skyboxProgram_vertexAttribute = glGetAttribLocation(g_skyboxProgram.program, "a_vertex");
}