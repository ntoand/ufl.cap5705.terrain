void cam_pan(float vertDelta, float horizDelta);
void cam_move(float moveDelta);
void cam_rotY(float rotDeltaY);
void cam_rotX(float rotDeltaX);
void cam_buildViewMatrix(float viewMatrix[16]);