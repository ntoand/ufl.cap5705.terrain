//
// Main
//
extern GLfloat g_mMatrix[16];	// Model matrix
extern GLfloat g_vMatrix[16];	// View matrix
extern GLfloat g_pMatrix[16];	// Projection matrix
extern GLfloat g_mvMatrix[16];	// Movel view matrix
extern GLfloat g_mvpMatrix[16];	// Model-view-projection matrix
extern GLfloat g_nMatrix[9];	// Normal matrix

//
// Shader.c
//
extern GLUSprogram g_program;
extern GLuint g_program_vertexAttribute;
extern GLuint g_program_patchTexCoordAttribute;
void shader_programInit();

extern GLUSprogram g_skyboxProgram;
extern GLuint g_skyboxProgram_vertexAttribute;
void shader_skyboxProgramInit();


//
// texture.c
//
void tex_load(GLenum texture, int texNum, char *file, GLuint program, char* uniform);
void tex_loadMipmap(GLenum texture, int texNum, int mipLevels, char *file, GLuint program, char* uniform);
void tex_loadSkybox(GLenum texture, int texNum,
	char *filePosX, char *fileNegX, char *filePosY, char *fileNegY, char *filePosZ, char *fileNegZ,
	GLuint program, char* uniform);

typedef struct {
	char colorMap[50];
	char normalMap[50];
	int mipLevels;
	float softHeight;
	float hardHeight;

} tex_terrain_t;

//
//
//
typedef struct {
	char heightMap[50];
	char normalMap[50];

	tex_terrain_t texBase; // Base texture.
	tex_terrain_t tex0;
	tex_terrain_t tex1;
	tex_terrain_t tex2;
	tex_terrain_t tex3;

	int terrainWidth;	// East/West width in meters
	int terrainLength;	// North/south width in meters
	int terrainHeight;	// Heightmap is mapped from 0 to this value (in meters)
} scene_t;

//
// file.c
//
int file_loadScene(char* sceneFile, scene_t *scene);